# 3D Feature Pyramid Network and Time Domain Class Activation Intervals for Satellite Time Series - pytorch

This software addresses crop mapping by exploiting the time series of Sentinel-2 satellite images, with the specific aim to simultaneously extract information on "where and when" crops are grown.

The following figure is representative of the proposed technique for class activation interval
(CAI). The fully convolutive CNN trained for image segmentation is able to identify time intervals of the input time series that determines class presence. 
The CAI can be computed in a single forward step: for the class *maize* results show data relevance from May to October.
![class activation interval (CAI)](figures/growth_period_fig1.png)

## Architectures
The following figure shows of the proposed (3+2)D Feature Pyramid Network.
The green layers on the left represent the bottom-up pathway, here represented by a modified version of the ResNet 50.
The outputs of the last two 2D convolutions are used by the MSE Loss and the Cross-Entropy Loss to calculate the segmentation map and the class activation intervals.
![(3+2)D Feature Pyramid Network](figures/proposed-model.png)



```bibtex
@Article{gallo2021sentinel2,
  AUTHOR = {Gallo, Ignazio and La Grassa, Riccardo and Landro, Nicola and Boschetti, Mirco},
  TITLE = {Sentinel 2 Time Series Analysis with 3D Feature Pyramid Network and Time Domain Class Activation Intervals for Crop Mapping},
  JOURNAL = {ISPRS International Journal of Geo-Information},
  VOLUME = {10},
  YEAR = {2021},
  NUMBER = {7},
  ARTICLE-NUMBER = {483},
  URL = {https://www.mdpi.com/2220-9964/10/7/483},
  ISSN = {2220-9964},
  DOI = {10.3390/ijgi10070483}
}
```

## Getting started

### Python Dependencies

Pip environment
```
pip install -r requirements.txt
```


### Download Dataset, Tuning Results and Models

You can download the original dataset from [Rußwurm M., Körner M.](https://github.com/TUM-LMF/MTLCC-pytorch) project.
We also created and published on [Kaggle](https://www.kaggle.com/artelabsuper/sentinel2-munich480) the version of the dataset used in our paper .


## External Code

* Pytorch -- Multitemporal Land Cover Classification Network by [Rußwurm M., Körner M.](https://github.com/TUM-LMF/MTLCC-pytorch)


### Pre-trained models
To be configured on [pytorch.org/hub/](https://pytorch.org/hub/)...

### Command line examples to train and test a model

To start a training process on Munich480 dataset, using NDVI Loss.

```
python3.8 main.py --loss ndvi --ignore_index 0 --batch_size 2 --optimizer sgd --model_depth 101 --result_path results_resnet101_ndviloss_batch2/ --sample_duration 30  --gpu_id 1 --learning_rate 0.01 --n_epochs 300  --root_path /datasets-nas/sentinel2/munich480 
```

To load the pretrained model *best_model.pth* and extract the Class Activation Interval from ndvi loss layer.

```
python3.8 main.py --test_only --resume_path results_resnet101_ndviloss_batch2/best_model.pth --loss ndvi --ignore_index 0 --batch_size 2 --model_depth 101 --result_path results_resnet101_ndviloss_batch2/ --sample_duration 30  --gpu_id 1 --root_path /datasets-nas/sentinel2/munich480 
```
With this configuration we have obtained the following confusion matrix using the *test_fold0* dataset:


| class |   |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
| ------------------------ |  ----- | ----- | ------ | ------ | ----- | ---- | ----- | ----- | ---- | ----- | ---- | --- | ------ | ------ | ----- | ----- | ------ |
| sugar\_beet(18783)       |  17597 | 0     | 68     | 25     | 0     | 13   | 11    | 0     | 3    | 72    | 0    | 2   | 54     | 44     | 0     | 46    | 848    |
| summer\_oat(22775)       |  8     | 15815 | 901    | 74     | 71    | 7    | 38    | 232   | 94   | 143   | 0    | 0   | 1058   | 83     | 4     | 3610  | 637    |
| meadow(149003)           |  96    | 522   | 130090 | 778    | 1480  | 191  | 175   | 126   | 163  | 1553  | 147  | 34  | 4472   | 1587   | 69    | 430   | 7090   |
| rape(104936)             |  11    | 12    | 420    | 102524 | 149   | 18   | 64    | 25    | 12   | 19    | 0    | 2   | 720    | 294    | 3     | 21    | 642    |
| hop(71411)               |  3     | 87    | 1059   | 133    | 66362 | 8    | 3     | 13    | 0    | 85    | 0    | 344 | 403    | 272    | 39    | 0     | 2600   |
| winter\_spelt(17006)     |  7     | 7     | 635    | 92     | 25    | 8693 | 1039  | 18    | 0    | 0     | 0    | 20  | 5229   | 291    | 531   | 290   | 129    |
| winter\_triticale(33670) |  1     | 53    | 2269   | 129    | 71    | 654  | 13650 | 9     | 6    | 20    | 69   | 0   | 10080  | 5053   | 886   | 281   | 439    |
| beans(15207)             |  0     | 1310  | 230    | 34     | 59    | 0    | 0     | 12189 | 79   | 533   | 0    | 0   | 235    | 50     | 0     | 169   | 319    |
| peas(9008)               |  22    | 447   | 189    | 46     | 21    | 5    | 14    | 37    | 6150 | 378   | 7    | 95  | 221    | 43     | 4     | 1159  | 170    |
| potatoe(74040)           |  66    | 46    | 1224   | 183    | 367   | 52   | 2     | 117   | 198  | 68167 | 48   | 21  | 1141   | 190    | 0     | 362   | 1856   |
| soybeans(11944)          |  16    | 1     | 152    | 33     | 0     | 1    | 3     | 0     | 17   | 1552  | 9658 | 0   | 51     | 72     | 0     | 3     | 385    |
| asparagus(835)           |  0     | 0     | 1      | 0      | 0     | 0    | 0     | 0     | 0    | 40    | 0    | 756 | 0      | 7      | 0     | 0     | 31     |
| winter\_wheat(581700)    |  137   | 359   | 2224   | 983    | 197   | 2824 | 3272  | 44    | 37   | 418   | 29   | 2   | 562703 | 2710   | 386   | 1061  | 4314   |
| winter\_barley(213605)   |  24    | 71    | 619    | 379    | 137   | 36   | 939   | 44    | 34   | 199   | 33   | 8   | 3234   | 204633 | 57    | 506   | 2652   |
| winter\_rye(15314)       |  11    | 15    | 319    | 338    | 41    | 1018 | 1231  | 4     | 2    | 25    | 0    | 1   | 1284   | 659    | 10142 | 86    | 138    |
| summer\_barley(51584)    |  43    | 1214  | 509    | 266    | 7     | 18   | 37    | 222   | 201  | 326   | 0    | 0   | 1884   | 486    | 16    | 45825 | 530    |
| maize(712980)            |  1060  | 106   | 3341   | 635    | 787   | 23   | 189   | 50    | 79   | 1070  | 84   | 103 | 3668   | 1746   | 30    | 250   | 699759 |

and the following measures:
* Over All (OA):	0.9386
* Kappa:	0.9217
* Weighted Recall:	0.9386
* Weighted Precision:	0.9360
* Weighted F-measure:	0.9373



## External Code

* Pytorch -- Multitemporal Land Cover Classification Network by [Rußwurm M., Körner M.](https://github.com/TUM-LMF/MTLCC-pytorch)




