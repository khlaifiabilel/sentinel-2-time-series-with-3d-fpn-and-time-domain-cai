import torch
from math import cos, pi
import numpy as np
from sklearn.metrics import f1_score, precision_score, recall_score, jaccard_score, accuracy_score, confusion_matrix
import warnings
warnings.filterwarnings('ignore')


def adjust_classes_weights(cur_epoch, curr_iter, num_iter_x_epoch, tot_epochs, start_w, descending=True):
    current_iter = curr_iter + cur_epoch * num_iter_x_epoch
    max_iter = tot_epochs * num_iter_x_epoch
    # a = 0.75 - current_iter / (2 * max_iter)  # from 0.75 to 0.25
    a = 1 - current_iter / (max_iter)  # from 0 to 1
    if not descending:
        a = 1 - a    # from 0.25 to 0.75

    w = a * start_w + 1 - a
    return w


def adjust_learning_rate(cur_epoch, curr_iter, num_iter_x_epoch, tot_epochs, start_lr, lr_decay='cos'):
    current_iter = curr_iter + cur_epoch * num_iter_x_epoch
    max_iter = tot_epochs * num_iter_x_epoch

    if lr_decay == 'cos':
        lr = start_lr * (1 + cos(pi * (current_iter) / (max_iter))) / 2
    # elif lr_decay == 'step':
    #     lr = start_lr * (0.1 ** (cur_epoch // args.step))
    elif lr_decay == 'linear':
        lr = start_lr * (1 - (current_iter) / (max_iter))
    # elif lr_decay == 'schedule':
    #     count = sum([1 for s in args.schedule if s <= cur_epoch])
    #     lr = start_lr * pow(args.gamma, count)
    else:
        raise ValueError('Unknown lr mode {}'.format(lr_decay))

    return lr
    # for param_group in optimizer.param_groups:
    #     param_group['lr'] = lr


def _take_channels(*xs, ignore_channels=None):
    if ignore_channels is None:
        return xs
    else:
        channels = [channel for channel in range(xs[0].shape[1]) if channel not in ignore_channels]
        xs = [torch.index_select(x, dim=1, index=torch.tensor(channels).to(x.device)) for x in xs]
        return xs


def _remove_index(np_matrix, ignore_index=-100):
    if ignore_index == -100:
        return np_matrix
    else:
        m = np.delete(np_matrix, ignore_index, 0) # axes=0 -> delete row index
        m = np.delete(m, ignore_index, 1) # axes=1 -> delete col index
        return m


def _threshold(x, threshold=None):
    if threshold is not None:
        return (x > threshold).type(x.dtype)
    else:
        return x


def iou(pr, gt, labels, threshold=None, ignore_channels=None):
    """Computes the IoU and mean IoU.
    The mean computation ignores NaN elements of the IoU array.
    Returns:
        Tuple: (IoU, mIoU). The first output is the per class IoU,
        for K classes it's numpy.ndarray with K elements. The second output,
        is the mean IoU.
    """
    # Dimensions check
    assert pr.size(0) == gt.size(0), \
        'number of targets and predicted outputs do not match'
    assert pr.dim() == gt.dim(), \
        "predictions and targets must be of dimension (N, H, W)"

    pr = _threshold(pr, threshold=threshold)
    pr, gt = _take_channels(pr, gt, ignore_channels=ignore_channels)

    # sklearn.metrics
    pr1d = pr.view(-1).cpu().detach().numpy()
    gt1d = gt.view(-1).cpu().detach().numpy()
    score = jaccard_score(gt1d, pr1d, labels=labels, average=None)

    return score
    # conf_metric = ConfusionMatrix(num_classes, normalized)
    # conf_metric.add(pr.view(-1), gt.view(-1))
    # conf_matrix = conf_metric.value()
    # if ignore_channels is not None:
    #     conf_matrix[:, ignore_channels] = 0
    #     conf_matrix[ignore_channels, :] = 0
    # true_positive = np.diag(conf_matrix)
    # false_positive = np.sum(conf_matrix, 0) - true_positive
    # false_negative = np.sum(conf_matrix, 1) - true_positive
    #
    # # Just in case we get a division by 0, ignore/hide the error
    # with np.errstate(divide='ignore', invalid='ignore'):
    #     iou = true_positive / (true_positive + false_positive + false_negative)
    #
    # return iou, np.nanmean(iou)


def f_score(pr, gt, labels, threshold=None, ignore_channels=None):
    """Calculate F-score between ground truth and prediction
    Args:
        pr (torch.Tensor): predicted tensor
        gt (torch.Tensor):  ground truth tensor
        threshold: threshold for outputs binarization
    Returns:
        numpy array float: F score
    """

    pr = _threshold(pr, threshold=threshold)
    pr, gt = _take_channels(pr, gt, ignore_channels=ignore_channels)

    # sklearn.metrics
    pr1d = pr.view(-1).cpu().detach().numpy()
    gt1d = gt.view(-1).cpu().detach().numpy()
    score = f1_score(gt1d, pr1d, labels=labels, average=None)

    # tp = torch.sum(gt * pr)
    # fp = torch.sum(pr) - tp
    # fn = torch.sum(gt) - tp
    #
    # score = ((1 + beta ** 2) * tp + eps) \
    #         / ((1 + beta ** 2) * tp + beta ** 2 * fn + fp + eps)

    return score


def accuracy(pr, gt, labels, threshold=None, ignore_channels=None, ignore_index=-100):
    """Calculate accuracy score between ground truth and prediction
    Args:
        pr (torch.Tensor): predicted tensor
        gt (torch.Tensor):  ground truth tensor
        eps (float): epsilon to avoid zero division
        threshold: threshold for outputs binarization
    Returns:
        float: precision score
    """
    pr = _threshold(pr, threshold=threshold)
    pr, gt = _take_channels(pr, gt, ignore_channels=ignore_channels)

    # sklearn.metrics
    pr1d = pr.view(-1).cpu().detach().numpy()
    gt1d = gt.view(-1).cpu().detach().numpy()

    matrix = confusion_matrix(gt1d, pr1d, labels=labels)
    matrix = _remove_index(matrix, ignore_index)
    diagonal = matrix.diagonal()
    score_per_class = np.nan_to_num(diagonal / matrix.sum(axis=1))

    score = diagonal.sum() / matrix.sum()

    # corrects = (gt == pr).float()
    # score = corrects.sum() / float(corrects.numel())

    return score_per_class, score


def precision(pr, gt, labels, threshold=None, ignore_channels=None):
    """Calculate precision score between ground truth and prediction
    Args:
        pr (torch.Tensor): predicted tensor
        gt (torch.Tensor):  ground truth tensor
        eps (float): epsilon to avoid zero division
        threshold: threshold for outputs binarization
    Returns:
        numpy array: precision score
    """
    pr = _threshold(pr, threshold=threshold)
    pr, gt = _take_channels(pr, gt, ignore_channels=ignore_channels)

    # sklearn.metrics
    pr1d = pr.view(-1).cpu().detach().numpy()
    gt1d = gt.view(-1).cpu().detach().numpy()
    score = precision_score(gt1d, pr1d, labels=labels, average=None)

    return score


def recall(pr, gt, labels, threshold=None, ignore_channels=None):
    """Calculate Recall between ground truth and prediction
    Args:
        pr (torch.Tensor): A list of predicted elements
        gt (torch.Tensor):  A list of elements that are to be predicted
        eps (float): epsilon to avoid zero division
        threshold: threshold for outputs binarization
    Returns:
        numpy array float: recall score
    """

    pr = _threshold(pr, threshold=threshold)
    pr, gt = _take_channels(pr, gt, ignore_channels=ignore_channels)

    # sklearn.metrics
    pr1d = pr.view(-1).cpu().detach().numpy()
    gt1d = gt.view(-1).cpu().detach().numpy()
    score = recall_score(gt1d, pr1d, labels=labels, average=None)  # per class measure

    return score
